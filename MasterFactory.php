<?php
/*
MasterFactory.php
A master factory abstract class that provides for handling entities and their associated CRUD views
Copyright (C) 2016,2017 Daniel Fredriksen
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
namespace CYINT\ComponentsPHP\Factory;

use CYINT\ComponentsPHP\Classes\ParseData;
use Doctrine\Common\Collections\ArrayCollection;

abstract class MasterFactory
{
    protected $Repository;
    protected $Doctrine;
    protected $Manager;
    protected $fields;
    protected $_locale;
    protected $parentid;
    protected $settings;
    protected $EntityType;
    protected $accepted_import_extensions;

    public function __construct($Repository, $Doctrine, $Manager)
    {       
        if(empty($this->getDoctrine()))
            $this->setDoctrine($Doctrine);    

        if(empty($this->getRepository()))
            $this->setRepository($Repository);

        if(empty($this->getManager()))
            $this->setManager($Manager);

        $this->setAcceptedImportExtensions(['xlsx']);
        $this->setLocale('en');
        $this->setParentId(null);
    }

    protected function entityEditUnique(&$Entity) {}
    public function prepareUniqueData() {}
    public function postProcessing($Entity) { }

    public function constructEntityFromData($form_data, &$Entity = null, $_locale = 'en', $parentid = null)
    {
        $this->setLocale($_locale);
        $this->setParentId($parentid);
        $this->processForm($form_data);
        $this->validateForm($form_data);
        if(empty($Entity))
        {
            $this->entityConstruction($Entity);
        }
        else
        {
            if(method_exists($Entity, 'setLocaleField'))
                $Entity->setLocaleField($this->getLocale());

            $this->setEntityProperties($Entity);
        }

        return $this->fields;       
    }


    public function processForm($form_data)
    {
        foreach($this->fields as $key=>&$field)
        {       
            switch( $field['type'] )
            {
                case 'text':
                case 'textarea':
                case 'richtext':
                case 'image':
                case 'email':         
                    $field['value'] = ParseData::setArray($form_data,$key,$field['default']);
                    $field['value'] = trim($field['value']);
                break;
                case 'checkbox':
                    $field['value'] = ParseData::setArray($form_data,$key,$field['default']);
                    $field['value'] = !empty($field['value']);
                break;            
                case 'datetime':
                    $date_string = ParseData::setArray($form_data,$key . '_date', date('Y-m-d')) . ' ' . ParseData::setArray($form_data, $key . '_hours', date('H')) . ':' . ParseData::setArray($form_data, $key . '_minutes', date('i'));
                    $timestamp = strtotime($date_string);
                    $offset = ParseData::setArray($form_data,$key . '_offset', 0);           
                    $field['value'] = $timestamp + (3600 * $offset * -1);
                break;
                case 'date':
                    $date_string = ParseData::setArray($form_data,$key, date('Y-m-d'));
                    $timestamp = strtotime($date_string);
                    $offset = ParseData::setArray($form_data,$key . '_offset', 0);           
                    $field['value'] = $timestamp + (3600 * $offset * -1);
                break;
                case 'select':                    
                    $field['value'] = ParseData::setArray($form_data,$key,$field['default']);
                break;
                default:
                    $field['value'] = ParseData::setArray($form_data,$key,$field['default']);
                break;
            }                    
        }
    }

    public function validateForm($form_data)
    {
        $message = '';
        foreach($this->fields as $key=>&$field)
        {
            if(!empty($field['validation']))
            {            
                foreach($field['validation'] as $validation)
                {
                    switch( $validation )
                    {
                        case 'min':
                            if($field['type'] == 'datetime')                                                    
                            {
                                $offset = ParseData::setArray($form_data,$key . '_offset', 0); 
                                $value = $field['value'] + (-1 * 3600 * $offset);
                                if($value < $field['min'])                            
                                    $message .= $field['label'] . ' must be greater than <span class="js-date" data-timestamp="' . $field['min'] . '" data-format="M/D/YYYY HH:MM"></span>';                                     
                            }    
                            else
                                if($field['value'] < $field['min'])
                                    $message .= $field['label'] . ' must be greater than ' . $field['min'];                        
                        break;
                        case 'required':
                            if($field['value'] === null)
                                $message .= $field['label'] . ' is a required field.<br />';
                        break;
                    }                    
                }
            }
        }

        if(!empty($message))
            throw new \Exception($message);
    }

    public function prepareEntityFieldData($_locale = 'en', $Entity, $parentid = null)
    {
        $this->setLocale($_locale);
        $this->setParentId($parentid);

        if(method_exists($Entity, 'setLocaleField'))
            $Entity->setLocaleField($this->getLocale());

        if(!empty($Entity))
        {
            foreach($this->fields as $key=>&$field)
            {   
                if(empty($field['getter']))
                    $getter = 'get' . $key;            
                else
                    $getter = $field['getter'];

                if($field['type'] != 'select' || $field['selectOptions']['type'] != 'entity')
                    $field['value'] = $Entity->$getter();          
                else
                {
                    $SelectedEntity = null; 
                    $SelectedEntity = $Entity->$getter();
                                       
                    $selectedGetter = $field['selectOptions']['valueGetter'];

                    if(!empty($field['selectOptions']['multiple']))
                    {
                        foreach($SelectedEntity as $SelectedEntityItem)
                        {
                            $field['value'][] = $SelectedEntityItem->$selectedGetter();          
                        }
                    }
                    else
                    {
                        if(!empty($SelectedEntity))
                            $field['value'] = $SelectedEntity->$selectedGetter();          
                    }
                }
            }
        }

    }


    public function getSuccessMessage($create = true)
    {
        if($create)
            return "The entity has been created successfully.";
        else
            return "The entity has been updated successfully.";
    }

    public function getExceptionMessage(\Exception $Ex = null)
    {     
        return $Ex->getMessage();
    }

    public function persistData($Entity)
    {
        $this->Manager->persist($Entity);
        $this->Manager->flush();
    }

    public function initializeField($type = 'text', $label = '', $value='', $default='', $validation=[], $extras=[]) 
    {
        $field = [
            'value' => $value
            ,'default' => $default
            ,'type' => $type
            ,'label' => $label
            ,'validation'=>$validation          
        ];

        if(!empty($extras))
            $field = array_merge($field, $extras);

        return $field;
    }

    public function entityConstruction(&$Entity)
    {                       
        $Entity = new $this->EntityType();                              
        $this->setEntityProperties($Entity);
    }

    public function setEntityProperties(&$Entity)
    {
        foreach($this->fields as $key=>$field)
        {
            $setter = 'set' . $key;
            if(
                $field['type'] != 'none' 
                && (!isset($field['selectOptions']) || $field['selectOptions']['type'] != 'entity')  
            )
            {
                $Entity->$setter($field['value']);
            }
            elseif(isset($field['selectOptions']) && $field['selectOptions']['type'] == 'entity')
            {
                $SelectedEntity = null;
                if(!empty($field['selectOptions']['multiple'])) 
                    $SelectedEntity = new ArrayCollection($this->getDoctrine()->getRepository($field['selectOptions']['repository'])->findBy(['id'=>$field['value']]));
                else
                    $SelectedEntity = $this->getDoctrine()->getRepository($field['selectOptions']['repository'])->find($field['value']); 

                if(empty($SelectedEntity) && in_array('required', $field['validation']))
                {
                    throw new \Exception('Could not find ' . $field['label'] . ' entity associated with this id', 500);
                }
                $Entity->$setter($SelectedEntity);
            }
        }

        $this->entityEditUnique($Entity);           

    }

    public function getEntityFieldData($_locale = 'en', $Entity, $parentid = null)
    {
        $values = [];
        if(!empty($Entity))
        {
            foreach($this->fieldKeys as $key=>$field)
            {
                $method = 'get' . $this->formatKeyAsMethod($field);
                $values[$field] = $Entity->$method();
            }      
        }

        return $values;
    }
    

    public function formatKeyAsMethod($key)
    {
		$method = str_replace('_', '', ucwords($key, '_'));
		$method = ucfirst($method);
		return $method;
    }

    public function setRepository($Repository)
    {
        $this->Repository = $Repository;
    }

    public function setDoctrine($Doctrine)
    {
        $this->Doctrine = $Doctrine;
    }

    public function setManager($Manager)
    {
        $this->Manager = $Manager;
    }

    public function getRepository()
    {
        return $this->Repository;
    }

    public function getDoctrine()
    {
        return $this->Doctrine;
    }

    public function getManager()
    {
        return $this->Manager;
    }

    public function setLocale($_locale)
    {
        $this->_locale = $_locale;
    }

    public function getLocale()
    {
        return $this->_locale;
    }

    public function setParentId($parentid)
    {
        $this->parentid = $parentid;        
    }

    public function getParentId()
    {
        return $this->parentid;
    }

    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getSettings()
    {
        return $this->settings;
    }   

    public function setSettings($settings)
    {
        $this->settings = $settings;
    }

    public function getFieldKeys() 
    {
        return array_keys($this->fields);
    }

    public function createEntityFromArray($value_map, $locale = null, $parentid = null)
    {        
        $Entity = null;

        $form_data = $this->convertValueMapToData($value_map);

        if(empty($form_data))
            return null;

        if(!empty($form_data['Id']))
            $Entity = $this->loadEntity($form_data['Id']);

        $this->constructEntityFromData($form_data, $Entity, $locale, $parentid);
        return $Entity;
    } 

    public function loadEntity($id)
    {
        return $this->Repository->find($id);
    }

    public function convertValueMapToData($value_map)
    {
        $form_data = [];
        foreach($value_map as $tuple)
        {
            if(empty($tuple['field']))
                continue;

            $form_data[$tuple['field']] = null;
            if($this->fields[$tuple['field']]['type'] == 'select' && !empty($this->fields[$tuple['field']]['selectOptions']) && !empty($this->fields[$tuple['field']]['selectOptions']['multiple']))
            {
                $form_data[$tuple['field']] = (stristr($tuple['value'], ',') > -1) 
                    ? explode($tuple['value']) : empty($tuple['value']) 
                        ? $form_data[$tuple['field']]['default'] : $tuple['value'];
            }
            else
                $form_data[$tuple['field']] = empty($tuple['value']) 
                    ? $form_data[$tuple['field']]['default'] : $tuple['value'];

            if($this->fields[$tuple['field']]['type'] == 'date' || $this->fields[$tuple['field']]['type'] == 'datetime')
            {
				$format = empty($this->fields[$tuple['field']]['format']) ? 'Y-m-d' : $this->fields[$tuple['field']]['format'];
				$timestring = $tuple['value'];
                 
				if($format != 'Y-m-d')
				{
	                $myDateTime = \DateTime::createFromFormat($format, $timestring);			
					$timestring = $myDateTime->format('Y-m-d');                               
				}
                $form_data[$tuple['field']] = $timestring;
            }
        }

        $empty = true;
        foreach($form_data as $value)
        {
            if(!empty(trim($value)))
            {
                $empty = false;
                break;
            }
        }

        if($empty)
            $form_data = null;
        return $form_data;
    }

    public function getAcceptedImportExtensions()
    {
        return $this->accepted_import_extensions;
    }

    public function setAcceptedImportExtensions($accepted_extensions)
    {
        $this->accepted_import_extensions = $accepted_extensions;
    }

    public function getEntityType()
    {
        return $this->EntityType;
    }

    public function convertFieldToString($field, $Entity)
    {
        $getter = empty($this->fields[$field]['getter']) ? 'get' . $field : $this->fields[$field]['getter'];
        $value = $Entity->$getter();
        if($this->fields[$field]['type'] == 'select' && $this->fields[$field]['selectOptions']['type'] == 'entity')
        {
            $values = [];
            $value = '';
            if(!empty($this->fields[$field]['selectOptions']['multiple']))
            {
                foreach($Entity->$getter() as $Item)
                {
                    $values[] = $Item->getId();
                }

                $value = implode(',',$values);
            }
            else
            {
                if(!empty($Entity->$getter()))
                    $value = $Entity->$getter()->getId();
            }
        }
        elseif($this->fields[$field]['type'] == 'date' || $this->fields[$field]['type'] == 'datetime')
        {
            $format = empty($this->fields[$field]['format']) ? 'Y-m-d' : $this->fields[$field]['format'];
            $value = date($format,$Entity->$getter());
        }

        return $value;
    }
}
